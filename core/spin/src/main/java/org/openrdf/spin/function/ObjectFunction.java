/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin.function;

import info.aduna.iteration.CloseableIteration;

import org.openrdf.model.IRI;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.vocabulary.SPL;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.algebra.evaluation.QueryPreparer;
import org.openrdf.query.algebra.evaluation.ValueExprEvaluationException;
import org.openrdf.query.algebra.evaluation.function.Function;

public class ObjectFunction extends AbstractSpinFunction implements Function {

	public ObjectFunction() {
		super(SPL.OBJECT_FUNCTION.toString());
	}

	@Override
	public Value evaluate(ValueFactory valueFactory, Value... args)
		throws ValueExprEvaluationException
	{
		QueryPreparer qp = getCurrentQueryPreparer();
		if (args.length != 2) {
			throw new ValueExprEvaluationException(String.format("%s requires 2 arguments, got %d", getURI(), args.length));
		}
		Value subj = args[0];
		if(!(subj instanceof Resource)) {
			throw new ValueExprEvaluationException("First argument must be a subject");
		}
		Value pred = args[1];
		if(!(pred instanceof IRI)) {
			throw new ValueExprEvaluationException("Second argument must be a predicate");
		}

		try {
			CloseableIteration<? extends Statement, QueryEvaluationException> stmts = qp.getTripleSource().getStatements((Resource) subj, (IRI) pred, null);
			try {
				if(stmts.hasNext()) {
					return stmts.next().getObject();
				}
				else {
					throw new ValueExprEvaluationException("No value");
				}
			}
			finally {
				stmts.close();
			}
		}
		catch(QueryEvaluationException e) {
			throw new ValueExprEvaluationException(e);
		}
	}
}
