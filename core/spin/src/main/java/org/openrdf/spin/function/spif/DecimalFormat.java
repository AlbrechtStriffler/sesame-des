/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin.function.spif;

import org.openrdf.model.Literal;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.vocabulary.SPIF;
import org.openrdf.model.vocabulary.XMLSchema;
import org.openrdf.query.algebra.evaluation.ValueExprEvaluationException;
import org.openrdf.query.algebra.evaluation.function.BinaryFunction;

public class DecimalFormat extends BinaryFunction {

	@Override
	public String getURI() {
		return SPIF.DECIMAL_FORMAT_FUNCTION.toString();
	}

	@Override
	protected Value evaluate(ValueFactory valueFactory, Value arg1, Value arg2)
		throws ValueExprEvaluationException
	{
		if (!(arg1 instanceof Literal) || !(arg2 instanceof Literal)) {
			throw new ValueExprEvaluationException("Both arguments must be literals");
		}
		Literal number = (Literal) arg1;
		Literal format = (Literal) arg2;

		java.text.DecimalFormat formatter = new java.text.DecimalFormat(format.getLabel());
		String value;
		if (XMLSchema.INT.equals(number.getDatatype())
			|| XMLSchema.LONG.equals(number.getDatatype())
			|| XMLSchema.SHORT.equals(number.getDatatype())
			|| XMLSchema.BYTE.equals(number.getDatatype())) {
			value = formatter.format(number.longValue());
		}
		else if (XMLSchema.DECIMAL.equals(number.getDatatype())) {
			value = formatter.format(number.decimalValue());
		}
		else if (XMLSchema.INTEGER.equals(number.getDatatype())) {
			value = formatter.format(number.integerValue());
		}
		else {
			value = formatter.format(number.doubleValue());
		}
		return valueFactory.createLiteral(value);
	}
}
