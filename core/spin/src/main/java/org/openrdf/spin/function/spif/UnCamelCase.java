/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin.function.spif;

import org.openrdf.model.Literal;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.vocabulary.SPIF;
import org.openrdf.query.algebra.evaluation.ValueExprEvaluationException;
import org.openrdf.query.algebra.evaluation.function.UnaryFunction;

public class UnCamelCase extends UnaryFunction {

	@Override
	public String getURI() {
		return SPIF.UN_CAMEL_CASE_FUNCTION.toString();
	}

	@Override
	protected Value evaluate(ValueFactory valueFactory, Value arg) throws ValueExprEvaluationException {
		if (!(arg instanceof Literal)) {
			throw new ValueExprEvaluationException("Argument must be a string");
		}
		String s = ((Literal)arg).getLabel();
		StringBuilder buf = new StringBuilder(s.length() + 10);
		char prev = '\0';
		for (int i=0; i<s.length(); i++) {
			char ch = s.charAt(i);
			if (Character.isLowerCase(prev) && Character.isUpperCase(ch)) {
				buf.append(' ');
				buf.append(Character.toLowerCase(ch));
			}
			else if (ch == '_') {
				buf.append(' ');
			}
			else {
				buf.append(ch);
			}
			prev = ch;
		}
		return valueFactory.createLiteral(buf.toString());
	}
}
