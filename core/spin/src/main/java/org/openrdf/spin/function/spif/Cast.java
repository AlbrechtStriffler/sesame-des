/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin.function.spif;

import org.openrdf.model.IRI;
import org.openrdf.model.Literal;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.vocabulary.SPIF;
import org.openrdf.query.algebra.evaluation.ValueExprEvaluationException;
import org.openrdf.query.algebra.evaluation.function.BinaryFunction;
import org.openrdf.query.algebra.evaluation.function.Function;
import org.openrdf.query.algebra.evaluation.function.FunctionRegistry;

public class Cast extends BinaryFunction {

	@Override
	public String getURI() {
		return SPIF.CAST_FUNCTION.toString();
	}

	@Override
	protected Value evaluate(ValueFactory valueFactory, Value arg1, Value arg2)
		throws ValueExprEvaluationException
	{
		if (!(arg1 instanceof Literal)) {
			throw new ValueExprEvaluationException("First argument must be a literal");
		}
		if (!(arg2 instanceof IRI)) {
			throw new ValueExprEvaluationException("Second argument must be a datatype");
		}
		Literal value = (Literal) arg1;
		IRI targetDatatype = (IRI) arg2;
		Function func = FunctionRegistry.getInstance().get(targetDatatype.stringValue()).orElse(null);
		return (func != null) ? func.evaluate(valueFactory, value) : valueFactory.createLiteral(value.getLabel(), targetDatatype);
	}
}
