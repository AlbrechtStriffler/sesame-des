/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.spin.function.spif;

import org.openrdf.model.Literal;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.vocabulary.SPIF;
import org.openrdf.query.algebra.evaluation.ValueExprEvaluationException;
import org.openrdf.query.algebra.evaluation.function.Function;

public class LastIndexOf implements Function {

	@Override
	public String getURI() {
		return SPIF.LAST_INDEX_OF_FUNCTION.toString();
	}

	@Override
	public Value evaluate(ValueFactory valueFactory, Value... args) throws ValueExprEvaluationException {
		if (args.length < 2 || args.length > 3) {
			throw new ValueExprEvaluationException("Incorrect number of arguments");
		}
		if (!(args[0] instanceof Literal)) {
			throw new ValueExprEvaluationException("First argument must be a string");
		}
		if (!(args[1] instanceof Literal)) {
			throw new ValueExprEvaluationException("Second argument must be a string");
		}
		if (args.length == 3 && !(args[2] instanceof Literal)) {
			throw new ValueExprEvaluationException("Third argument must be an integer");
		}
		Literal s = (Literal) args[0];
		Literal t = (Literal) args[1];
		int pos = (args.length == 3) ? ((Literal) args[2]).intValue() : s.getLabel().length();
		int index = s.getLabel().lastIndexOf(t.getLabel(), pos);
		if (index == -1) {
			throw new ValueExprEvaluationException("Substring not found");
		}
		return valueFactory.createLiteral(index);
	}
}
