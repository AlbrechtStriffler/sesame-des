/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.sail.lucene4.config;

import org.openrdf.sail.config.SailImplConfig;
import org.openrdf.sail.lucene.config.AbstractLuceneSailConfig;

/**
 * Config for full-text indexing Sail using Lucene 4.
 *
 * @author Jeen Broekstra
 * @deprecated since 4.1.0. Use the LuceneSail in package
 *             {@code org.openrdf.sail.lucene} instead.
 */
@Deprecated
public class LuceneSailConfig extends AbstractLuceneSailConfig {

	/*--------------*
	 * Constructors *
	 *--------------*/

	@Deprecated
	public LuceneSailConfig() {
		super(LuceneSailFactory.SAIL_TYPE);
	}

	@Deprecated
	public LuceneSailConfig(SailImplConfig delegate) {
		super(LuceneSailFactory.SAIL_TYPE, delegate);
	}

	@Deprecated
	public LuceneSailConfig(String luceneDir) {
		super(LuceneSailFactory.SAIL_TYPE, luceneDir);
	}

	@Deprecated
	public LuceneSailConfig(String luceneDir, SailImplConfig delegate) {
		super(LuceneSailFactory.SAIL_TYPE, luceneDir, delegate);
	}
}
