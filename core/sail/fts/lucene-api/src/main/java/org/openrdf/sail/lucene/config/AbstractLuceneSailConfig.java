/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.sail.lucene.config;

import static org.openrdf.sail.lucene.config.LuceneSailConfigSchema.INDEX_DIR;

import java.util.Properties;
import java.util.Set;

import org.openrdf.model.Literal;
import org.openrdf.model.Model;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.SimpleValueFactory;
import org.openrdf.model.util.Models;
import org.openrdf.sail.config.AbstractDelegatingSailImplConfig;
import org.openrdf.sail.config.SailConfigException;
import org.openrdf.sail.config.SailImplConfig;

public abstract class AbstractLuceneSailConfig extends AbstractDelegatingSailImplConfig {
	/*-----------*
	 * Variables *
	 *-----------*/

	private String indexDir;

	private Properties parameters = new Properties();

	/*--------------*
	 * Constructors *
	 *--------------*/

	protected AbstractLuceneSailConfig(String type) {
		super(type);
	}

	protected AbstractLuceneSailConfig(String type, SailImplConfig delegate) {
		super(type, delegate);
	}

	protected AbstractLuceneSailConfig(String type, String luceneDir) {
		super(type);
		setIndexDir(luceneDir);
	}

	protected AbstractLuceneSailConfig(String type, String luceneDir, SailImplConfig delegate) {
		super(type, delegate);
		setIndexDir(luceneDir);
	}

	/*---------*
	 * Methods *
	 *---------*/

	public String getIndexDir() {
		return indexDir;
	}

	public void setIndexDir(String luceneDir) {
		this.indexDir = luceneDir;
	}

	public String getParameter(String key) {
		return parameters.getProperty(key);
	}

	public void setParameter(String key, String value) {
		parameters.setProperty(key, value);
	}

	public Set<String> getParameterNames() {
		return parameters.stringPropertyNames();
	}

	@Override
	public Resource export(Model m) {
		Resource implNode = super.export(m);

		ValueFactory vf = SimpleValueFactory.getInstance();
		if (indexDir != null) {
			m.add(implNode, INDEX_DIR, SimpleValueFactory.getInstance().createLiteral(indexDir));
		}

		for (String key : getParameterNames()) {
			m.add(implNode, vf.createIRI(LuceneSailConfigSchema.NAMESPACE, key),
					vf.createLiteral(getParameter(key)));
		}

		return implNode;
	}

	@Override
	public void parse(Model graph, Resource implNode)
		throws SailConfigException
	{
		super.parse(graph, implNode);

		Literal indexDirLit = Models.objectLiteral(graph.filter(implNode, INDEX_DIR, null)).orElseThrow(
				() -> new SailConfigException("no value found for " + INDEX_DIR));

		setIndexDir(indexDirLit.getLabel());
		for (Statement stmt : graph.filter(implNode, null, null)) {
			if (stmt.getPredicate().getNamespace().equals(LuceneSailConfigSchema.NAMESPACE)) {
				if (stmt.getObject() instanceof Literal) {
					String key = stmt.getPredicate().getLocalName();
					setParameter(key, stmt.getObject().stringValue());
				}
			}
		}
	}
}
