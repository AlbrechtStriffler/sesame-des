/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.sail.spin;

import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.algebra.evaluation.QueryPreparer;
import org.openrdf.spin.QueryContext;

import info.aduna.iteration.CloseableIteration;

public class QueryContextIteration implements CloseableIteration<BindingSet, QueryEvaluationException> {

	private final CloseableIteration<? extends BindingSet, QueryEvaluationException> iter;
	private final QueryPreparer queryPreparer;

	public QueryContextIteration(CloseableIteration<? extends BindingSet, QueryEvaluationException> iter, QueryPreparer queryPreparer) {
		this.iter = iter;
		this.queryPreparer = queryPreparer;
	}

	@Override
	public boolean hasNext() throws QueryEvaluationException {
		QueryContext qctx = QueryContext.begin(queryPreparer);
		try {
			return iter.hasNext();
		}
		finally {
			qctx.end();
		}
	}

	@Override
	public BindingSet next() throws QueryEvaluationException {
		QueryContext qctx = QueryContext.begin(queryPreparer);
		try {
			return iter.next();
		}
		finally {
			qctx.end();
		}
	}

	@Override
	public void remove() throws QueryEvaluationException {
		QueryContext qctx = QueryContext.begin(queryPreparer);
		try {
			iter.remove();
		}
		finally {
			qctx.end();
		}
	}

	@Override
	public void close() throws QueryEvaluationException {
		QueryContext qctx = QueryContext.begin(queryPreparer);
		try {
			iter.close();
		}
		finally {
			qctx.end();
		}
	}
}
