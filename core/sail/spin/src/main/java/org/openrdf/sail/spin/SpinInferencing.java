/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.sail.spin;

import org.openrdf.OpenRDFException;
import org.openrdf.model.IRI;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.query.Binding;
import org.openrdf.query.BooleanQuery;
import org.openrdf.query.GraphQuery;
import org.openrdf.query.Operation;
import org.openrdf.query.Update;
import org.openrdf.query.algebra.evaluation.QueryPreparer;
import org.openrdf.query.algebra.evaluation.TripleSource;
import org.openrdf.query.parser.ParsedBooleanQuery;
import org.openrdf.query.parser.ParsedGraphQuery;
import org.openrdf.query.parser.ParsedOperation;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.ParsedUpdate;
import org.openrdf.sail.SailConnectionListener;
import org.openrdf.sail.inferencer.InferencerConnection;
import org.openrdf.sail.inferencer.util.RDFInferencerInserter;
import org.openrdf.spin.ConstraintViolation;
import org.openrdf.spin.ConstraintViolationRDFHandler;
import org.openrdf.spin.MalformedSpinException;
import org.openrdf.spin.ParsedTemplate;
import org.openrdf.spin.SpinParser;

/**
 * Collection of useful utilities for doing SPIN inferencing.
 */
public class SpinInferencing {

	private static final String THIS_VAR = "this";

	private SpinInferencing() {
	}

	public static int executeRule(Resource subj, Resource rule, QueryPreparer queryPreparer, SpinParser parser,
			InferencerConnection conn)
		throws OpenRDFException
	{
		int nofInferred;
		TripleSource tripleSource = queryPreparer.getTripleSource();
		ParsedOperation parsedOp = parser.parse(rule, tripleSource);
		if (parsedOp instanceof ParsedGraphQuery) {
			ParsedGraphQuery graphQuery = (ParsedGraphQuery)parsedOp;
			GraphQuery queryOp = queryPreparer.prepare(graphQuery);
			addBindings(subj, rule, graphQuery, queryOp, tripleSource, parser);
			CountingRDFInferencerInserter handler = new CountingRDFInferencerInserter(conn,
					tripleSource.getValueFactory());
			queryOp.evaluate(handler);
			nofInferred = handler.getStatementCount();
		}
		else if (parsedOp instanceof ParsedUpdate) {
			ParsedUpdate graphUpdate = (ParsedUpdate)parsedOp;
			Update updateOp = queryPreparer.prepare(graphUpdate);
			addBindings(subj, rule, graphUpdate, updateOp, tripleSource, parser);
			UpdateCountListener listener = new UpdateCountListener();
			conn.addConnectionListener(listener);
			updateOp.execute();
			conn.removeConnectionListener(listener);
			// number of statement changes
			nofInferred = listener.getAddedStatementCount() + listener.getRemovedStatementCount();
		}
		else {
			throw new MalformedSpinException("Invalid rule: " + rule);
		}
		return nofInferred;
	}

	public static ConstraintViolation checkConstraint(Resource subj, Resource constraint,
			QueryPreparer queryPreparer, SpinParser parser)
		throws OpenRDFException
	{
		ConstraintViolation violation;
		TripleSource tripleSource = queryPreparer.getTripleSource();
		ParsedQuery parsedQuery = parser.parseQuery(constraint, tripleSource);
		if (parsedQuery instanceof ParsedBooleanQuery) {
			ParsedBooleanQuery askQuery = (ParsedBooleanQuery)parsedQuery;
			BooleanQuery queryOp = queryPreparer.prepare(askQuery);
			addBindings(subj, constraint, askQuery, queryOp, tripleSource, parser);
			if (queryOp.evaluate()) {
				violation = parser.parseConstraintViolation(constraint, tripleSource);
			}
			else {
				violation = null;
			}
		}
		else if (parsedQuery instanceof ParsedGraphQuery) {
			ParsedGraphQuery graphQuery = (ParsedGraphQuery)parsedQuery;
			GraphQuery queryOp = queryPreparer.prepare(graphQuery);
			addBindings(subj, constraint, graphQuery, queryOp, tripleSource, parser);
			ConstraintViolationRDFHandler handler = new ConstraintViolationRDFHandler();
			queryOp.evaluate(handler);
			violation = handler.getConstraintViolation();
		}
		else {
			throw new MalformedSpinException("Invalid constraint: " + constraint);
		}
		return violation;
	}

	private static void addBindings(Resource subj, Resource opResource, ParsedOperation parsedOp, Operation op,
			TripleSource tripleSource, SpinParser parser)
		throws OpenRDFException
	{
		if (!parser.isThisUnbound(opResource, tripleSource)) {
			op.setBinding(THIS_VAR, subj);
		}
		if (parsedOp instanceof ParsedTemplate) {
			for (Binding b : ((ParsedTemplate)parsedOp).getBindings()) {
				op.setBinding(b.getName(), b.getValue());
			}
		}
	}

	private static class UpdateCountListener implements SailConnectionListener {

		private int addedCount;

		private int removedCount;

		@Override
		public void statementAdded(Statement st) {
			addedCount++;
		}

		@Override
		public void statementRemoved(Statement st) {
			removedCount++;
		}

		public int getAddedStatementCount() {
			return addedCount;
		}

		public int getRemovedStatementCount() {
			return removedCount;
		}
	}

	private static class CountingRDFInferencerInserter extends RDFInferencerInserter {

		private int stmtCount;

		public CountingRDFInferencerInserter(InferencerConnection con, ValueFactory vf) {
			super(con, vf);
		}

		@Override
		protected void addStatement(Resource subj, IRI pred, Value obj, Resource ctxt)
			throws OpenRDFException
		{
			super.addStatement(subj, pred, obj, ctxt);
			stmtCount++;
		}

		public int getStatementCount() {
			return stmtCount;
		}
	}
}
