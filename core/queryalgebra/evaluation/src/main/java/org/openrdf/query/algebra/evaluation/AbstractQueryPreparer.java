/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.query.algebra.evaluation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.query.BindingSet;
import org.openrdf.query.BooleanQuery;
import org.openrdf.query.Dataset;
import org.openrdf.query.GraphQuery;
import org.openrdf.query.GraphQueryResult;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryResults;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.query.TupleQueryResultHandler;
import org.openrdf.query.TupleQueryResultHandlerException;
import org.openrdf.query.Update;
import org.openrdf.query.UpdateExecutionException;
import org.openrdf.query.algebra.TupleExpr;
import org.openrdf.query.algebra.UpdateExpr;
import org.openrdf.query.impl.AbstractParserQuery;
import org.openrdf.query.impl.AbstractParserUpdate;
import org.openrdf.query.impl.GraphQueryResultImpl;
import org.openrdf.query.impl.TupleQueryResultImpl;
import org.openrdf.query.parser.ParsedBooleanQuery;
import org.openrdf.query.parser.ParsedGraphQuery;
import org.openrdf.query.parser.ParsedTupleQuery;
import org.openrdf.query.parser.ParsedUpdate;
import org.openrdf.rio.RDFHandler;
import org.openrdf.rio.RDFHandlerException;

import info.aduna.iteration.CloseableIteration;
import info.aduna.iteration.ConvertingIteration;
import info.aduna.iteration.FilterIteration;

public abstract class AbstractQueryPreparer implements QueryPreparer {

	private final TripleSource tripleSource;

	public AbstractQueryPreparer(TripleSource tripleSource) {
		this.tripleSource = tripleSource;
	}

	@Override
	public BooleanQuery prepare(ParsedBooleanQuery q) {
		return new BooleanQueryImpl(q);
	}

	@Override
	public TupleQuery prepare(ParsedTupleQuery q) {
		return new TupleQueryImpl(q);
	}

	@Override
	public GraphQuery prepare(ParsedGraphQuery q) {
		return new GraphQueryImpl(q);
	}

	@Override
	public Update prepare(ParsedUpdate u) {
		return new UpdateImpl(u);
	}

	@Override
	public TripleSource getTripleSource() {
		return tripleSource;
	}

	protected abstract CloseableIteration<? extends BindingSet, QueryEvaluationException> evaluate(TupleExpr tupleExpr, Dataset dataset, BindingSet bindings,
			boolean includeInferred, int maxExecutionTime) throws QueryEvaluationException;
	protected abstract void execute(UpdateExpr updateExpr, Dataset dataset, BindingSet bindings,
			boolean includeInferred, int maxExecutionTime) throws UpdateExecutionException;




	class BooleanQueryImpl extends AbstractParserQuery implements BooleanQuery {
		BooleanQueryImpl(ParsedBooleanQuery query) {
			super(query);
		}
	
		@Override
		public ParsedBooleanQuery getParsedQuery() {
			return (ParsedBooleanQuery)super.getParsedQuery();
		}
	
		@Override
		public boolean evaluate()
			throws QueryEvaluationException
		{
			ParsedBooleanQuery parsedBooleanQuery = getParsedQuery();
			TupleExpr tupleExpr = parsedBooleanQuery.getTupleExpr();
			Dataset dataset = getDataset();
			if (dataset == null) {
				// No external dataset specified, use query's own dataset (if any)
				dataset = parsedBooleanQuery.getDataset();
			}
	
			CloseableIteration<? extends BindingSet, QueryEvaluationException> bindingsIter;
			bindingsIter = AbstractQueryPreparer.this.evaluate(tupleExpr, dataset, getBindings(), getIncludeInferred(), getMaxExecutionTime());
			bindingsIter = enforceMaxQueryTime(bindingsIter);

			try {
				return bindingsIter.hasNext();
			}
			finally {
				bindingsIter.close();
			}
		}
	}

	class TupleQueryImpl extends AbstractParserQuery implements TupleQuery {
		TupleQueryImpl(ParsedTupleQuery query) {
			super(query);
		}

		@Override
		public ParsedTupleQuery getParsedQuery() {
			return (ParsedTupleQuery)super.getParsedQuery();
		}

		@Override
		public TupleQueryResult evaluate()
			throws QueryEvaluationException
		{
			TupleExpr tupleExpr = getParsedQuery().getTupleExpr();
			CloseableIteration<? extends BindingSet, QueryEvaluationException> bindingsIter;
			bindingsIter = AbstractQueryPreparer.this.evaluate(tupleExpr, getActiveDataset(), getBindings(), getIncludeInferred(), getMaxExecutionTime());
			bindingsIter = enforceMaxQueryTime(bindingsIter);
			return new TupleQueryResultImpl(new ArrayList<String>(tupleExpr.getBindingNames()), bindingsIter);
		}

		@Override
		public void evaluate(TupleQueryResultHandler handler)
			throws QueryEvaluationException, TupleQueryResultHandlerException
		{
			TupleQueryResult queryResult = evaluate();
			QueryResults.report(queryResult, handler);
		}
	}

	class GraphQueryImpl extends AbstractParserQuery implements GraphQuery {
		GraphQueryImpl(ParsedGraphQuery query) {
			super(query);
		}

		@Override
		public ParsedGraphQuery getParsedQuery() {
			return (ParsedGraphQuery)super.getParsedQuery();
		}

		@Override
		public GraphQueryResult evaluate()
			throws QueryEvaluationException
		{
			TupleExpr tupleExpr = getParsedQuery().getTupleExpr();
			CloseableIteration<? extends BindingSet, QueryEvaluationException> bindingsIter;
			bindingsIter = AbstractQueryPreparer.this.evaluate(tupleExpr, getActiveDataset(), getBindings(), getIncludeInferred(), getMaxExecutionTime());

			// Filters out all partial and invalid matches
			bindingsIter = new FilterIteration<BindingSet, QueryEvaluationException>(bindingsIter) {

				@Override
				protected boolean accept(BindingSet bindingSet) {
					Value context = bindingSet.getValue("context");

					return bindingSet.getValue("subject") instanceof Resource
							&& bindingSet.getValue("predicate") instanceof URI
							&& bindingSet.getValue("object") instanceof Value
							&& (context == null || context instanceof Resource);
				}
			};
			
			bindingsIter = enforceMaxQueryTime(bindingsIter);

			// Convert the BindingSet objects to actual RDF statements
			CloseableIteration<Statement, QueryEvaluationException> stIter;
			stIter = new ConvertingIteration<BindingSet, Statement, QueryEvaluationException>(bindingsIter) {
				private final ValueFactory vf = tripleSource.getValueFactory();

				@Override
				protected Statement convert(BindingSet bindingSet) {
					Resource subject = (Resource)bindingSet.getValue("subject");
					URI predicate = (URI)bindingSet.getValue("predicate");
					Value object = bindingSet.getValue("object");
					Resource context = (Resource)bindingSet.getValue("context");

					if (context == null) {
						return vf.createStatement(subject, predicate, object);
					}
					else {
						return vf.createStatement(subject, predicate, object, context);
					}
				}
			};

			return new GraphQueryResultImpl(getParsedQuery().getQueryNamespaces(), stIter);
		}

		@Override
		public void evaluate(RDFHandler handler)
				throws QueryEvaluationException, RDFHandlerException
		{
			GraphQueryResult queryResult = evaluate();
			QueryResults.report(queryResult, handler);
		}
	}

	class UpdateImpl extends AbstractParserUpdate {
		UpdateImpl(ParsedUpdate update) {
			super(update);
		}

		@Override
		public void execute()
			throws UpdateExecutionException
		{
			ParsedUpdate parsedUpdate = getParsedUpdate();
			List<UpdateExpr> updateExprs = parsedUpdate.getUpdateExprs();
			Map<UpdateExpr, Dataset> datasetMapping = parsedUpdate.getDatasetMapping();
			for (UpdateExpr updateExpr : updateExprs) {
				Dataset activeDataset = getMergedDataset(datasetMapping.get(updateExpr));

				try {
					AbstractQueryPreparer.this.execute(updateExpr, activeDataset, getBindings(), getIncludeInferred(), getMaxExecutionTime());
				}
				catch (UpdateExecutionException e) {
					if (!updateExpr.isSilent()) {
						throw e;
					}
				}
			}
		}
	}
}
