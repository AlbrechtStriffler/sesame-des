/* 
 * Licensed to Aduna under one or more contributor license agreements.  
 * See the NOTICE.txt file distributed with this work for additional 
 * information regarding copyright ownership. 
 *
 * Aduna licenses this file to you under the terms of the Aduna BSD 
 * License (the "License"); you may not use this file except in compliance 
 * with the License. See the LICENSE.txt file distributed with this work 
 * for the full License.
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS, 
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or 
 * implied. See the License for the specific language governing permissions
 * and limitations under the License.
 */
package org.openrdf.rio.helpers;

import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import org.openrdf.model.Statement;
import org.openrdf.rio.RDFHandler;
import org.openrdf.rio.RDFHandlerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TimeLimitRDFHandler extends RDFHandlerWrapper {
	private static final Timer timer = new Timer("TimeLimitRDFHandler", true);

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final InterruptTask interruptTask;

	private volatile boolean isInterrupted = false;

	private final AtomicBoolean ended = new AtomicBoolean(false);

	public TimeLimitRDFHandler(RDFHandler rdfHandler, long timeLimit) {
		super(rdfHandler);

		assert timeLimit > 0 : "time limit must be a positive number, is: " + timeLimit;

		interruptTask = new InterruptTask(this);

		timer.schedule(interruptTask, timeLimit);
	}

	private boolean isEnded() {
		return ended.get();
	}

	@Override
	public void startRDF() throws RDFHandlerException {
		checkInterrupted();
		super.startRDF();
	}

	@Override
	public void endRDF() throws RDFHandlerException {
		checkInterrupted();
		if (ended.compareAndSet(false, true)) {
			super.endRDF();
		}
	}

	@Override
	public void handleNamespace(String prefix, String uri) throws RDFHandlerException {
		checkInterrupted();
		super.handleNamespace(prefix, uri);
	}

	@Override
	public void handleStatement(Statement st) throws RDFHandlerException {
		checkInterrupted();
		super.handleStatement(st);
	}

	@Override
	public void handleComment(String comment) throws RDFHandlerException {
		checkInterrupted();
		super.handleComment(comment);
	}

	private void checkInterrupted() throws RDFHandlerException {
		if (isInterrupted) {
			throw new RDFHandlerException("RDFHandler took too long");
		}
	}
	private void interrupt() {
		isInterrupted = true;
		if(!isEnded()) {
			try {
				// we call endRDF() in case impls have resources to close
				endRDF();
			}
			catch (RDFHandlerException e) {
				logger.warn("Failed to end RDF", e);
			}
		}
	}

	private static class InterruptTask extends TimerTask {
		private WeakReference<TimeLimitRDFHandler> handlerRef;

		InterruptTask(TimeLimitRDFHandler handler) {
			this.handlerRef = new WeakReference<TimeLimitRDFHandler>(handler);
		}

		@Override
		public void run() {
			TimeLimitRDFHandler handler = handlerRef.get();
			if (handler != null) {
				handler.interrupt();
			}
		}
	}
}
